# nuLEDs Demo

## Overview

The demo consists of a computer running the adapter, the nuLEDs panel and a Whitebox.  The Whitebox luminosity sensor readings are connected to a rule in the rule engine.  When the luminosity changes significantly the rule sends a command to the adapter telling it to change the colour on the panel.  The adapter sends a message over the LAN to the panel.  Put your hand on the Whitebox sensor and the light changes white.  Take your hand away and it changes blue.

There is certainly room for improvement in the demo but this should give you a good starting point.

## Adapter

You'll need a computer that can run the Node.js adapter (Node.js 4.x).  It will need to be able to communicate with the nuLEDs panel via UDP.  It will also need internet access.

Clone the relayr/nuleds-adapter repo.  (I gave the Lab team read access.)  You'll need to create a config file in the app directory called .nuledsrc.  It will look like this (note that you can have as many devices as you like if you have multiple panels):

```json
{
  "devices": {
    "device_name": {
      // nuLEDs Demo on team@relayr.io
      "user": "c00fbdc9-b37e-4ea4-802c-798593f74a1a",
      "password": "xo46.I9ykU_s",
      "clientId": "TwA+9ybN+TqSALHmFk/dKGg",
      "address": "192.168.2.4",
      "port": 50000
    },
    "cl_device": {
      // nuLEDs Demo on ciscolive@relayr.io
      "user": "de9cbc45-2af8-4fea-b63d-84a0bbe90f02",
      "password": "p8saKBwyMyB6",
      "clientId": "T3py8RSr4T+q2PYSgu+kPAg",
      "address": "192.168.178.118",
      "port": 50000
    }
  }
}
```

To get the IP address of the light you'll need to scan the network (unless you already know what it is.)  In the following command you will likely need to change IP address range to match the one being used.  Run this from the adapter computer once the light is up and running on the network.  (It's looking for devices listening for UDP on port 50000 and then filtering the ones with a MAC address belonging to NuLEDs).

```bash
sudo nmap -sU -p50000 192.168.1.1-255 | grep -B4 NuLEDs
```

That should give you the IP address for the light.

Edit the ```.nuledsrc``` config file and change the IP address so that it points to the one found with the nmap scan.

From the nuleds-adapter directory run the following to start the adapter:

```bash
./node_modules/.bin/forever start app.js
```

You can see the logs in ```./logs/nuleds.log```

You can stop the adapter by running:

```bash
./node_modules/.bin/forever stopall
```

## Panel

I assume your panel is configured to use DHCP.  If it has a static address and you don't know what it is or need to change it I don't know how to do that.  There is a Windows app for managing the panels, presumably it can do that.  I can send you the app if needed.

The panel requires POE+ (ie: 802.3at Type 2) not just regular POE.  I think it will run on regular POE until you ask for too much power and then it crashes.

It can take minutes for the panel to boot up, especially if it is on regular POE.

## Whitebox

We've had some trouble getting the Whitebox to work with the POE from a Cisco switch so you may need an injector.  The default configuration on the Whitebox only sends data every 3 seconds or so.  This isn't great for a demo because the lag between covering up the sensor and the light changing can be pretty high.  Ideally you would reconfigure the Whitebox to send the data more often.  You can see a red LED inside the Whitebox flash when it actually sends the data.  When giving the demo you could point that out so that it doesn't seem so laggy...

The Whitebox requires internet access.

## Rule Engine

You'll need to create a rule to listen to the Whitebox and send commands to the nuLEDs Adapter.  Here is the rule I used for the Cisco Live demo:

```javascript
print(JSON.stringify(event))
return handle_reading(event, 'luminosity', function(value) {
  return on_changes('luminosity', value > 500, function(oldValue) {
    return DeviceCommand("de9cbc45-2af8-4fea-b63d-84a0bbe90f02", {
      'name': 'presets',
      'value': oldValue ? "White" : "Blue"
    })
  })
})
```

## Dashboard

I created a model for the nuLEDs demo.  It's on the ciscolive and team accounts.  You can copy it over to whatever account you're using.

I recommend creating a group for the demo and putting the panel and the Whitebox into it.  That way it's easy to see the two devices at the same time.

The model for the light has a command called "presets" which has a dropdown with a few options for changing the colour on the light.  Additionally covering the light sensor on the Whitebox with your hand should change the light to white, removing your hand should change it to blue.
