# Smart Scale 


##     Introduction 
    
In this project, a 0 to 20 kg scale was developed using a load cell and a WeMos D1 mini board. It is intended to work with any beverage crate. The scale is capable of connecting to the relayr cloud and sending the current values of weight, the number of bottles in the crate, the percentage of remaining bottles as well as the product that is being tracked currently. All this information is showed through the relayr Developer Dashboard.
    
##     Requirements

### Introductory concepts
1. [Connect ESP8266 based devices using Arduino](https://github.com/relayr/ESP8266_Arduino).

### Hardware

1. [WeMos D1 Mini Board (ESP8266)](http://www.wemos.cc/Products/d1_mini.html).
2. [Segor Load Cell](http://www.segor.de/#Q=WZ20A&M=1).
3. [Sparkfun Load Cell Amplifier - HX711](https://www.sparkfun.com/products/13879?_ga=1.163505404.150223461.1471007003).
4. 1 breadboard.
5. Color coded cables.

### Mechanical

* 2 wooden bases.
* 4 screws for fixing the cell to the wooden plates.
* 8 plain washers for fixing the cell to the wooden plates.
* 4 plastic feet for the base.
* 4 small screws for fixing the feet to the bases.


### Software

1. [Arduino sketch of this project](https://github.com/relayr/weekly-workshops/blob/master/biz-demos/smart-crate/ScaleDemo.ino).
2. [HX711 library](https://github.com/bogde/HX711).

##     Installation & Configuration: 
    
1. Follow the README.md file from the tutorial [connect ESP8266 based devices using Arduino](https://github.com/relayr/ESP8266_Arduino) and install the [USB - TTL driver](http://www.wemos.cc/downloads/), the [Arduino IDE](https://www.arduino.cc/en/Main/Software) and [Python 2.7](https://www.python.org/downloads/). Add the [board to the Arduino IDE](https://github.com/relayr/ESP8266_Arduino/blob/master/README.md#adding-the-board-to-the-arduino-ide) and install the from the [required libraries](https://github.com/relayr/ESP8266_Arduino/blob/master/README.md#required-libraries) the 'Arduino Client for MQTT' and 'Arduino JSON'.
2. Physically connect the WeMos board with the HX711 and the load cell using a breadboard and color coded cables.
3. Download the Arduino sketch with the code to be flashed over the WeMos D1 mini from GitHub and open it in the Arduino IDE.  
  **Note:** the sketch `smart-crate-with-credentials-and-calibration.ino` **includes all required values already**, and it's probably the most convenient option if you just want to reflash the WeMos board with the default configuration. However, if you want to replicate the demo from scratch, you may use `smart-crate.ino`, as it comes with place holders for the MQTT and WiFi credentials, as well as the calibration values.
4. Register in the [Developer Dashboard](dev.relayr.io) with your email address. Log in to your account and click on 'add a device'. Go to the 'by relayr' tab and select 'Smart Crate'. Add this device and click 'finish'.
5. Go to the  devices tab on the dashboard and find your added device. Open its properties and copy its mqtt credentials.
6. Go to the Arduino sketch opened in step (3). Change the WiFi credentials and the MQTT credentials according to your available wireless network and the credentials acquired in step (5).
7. Connect the WeMos D1 Mini board to your PC and inside the tools tab of the Arduino IDE select the appropriate serial port and the type of board you are using. In this case, it is the 'WeMos D1 Mini'.
8. Upload the updated code (with the appropriate WiFi and MQTT credentials) on your WeMos board.
9. Go to 'devices' tab on your Developer Dashboard.
10. Make some physical stress on the load cell and see the data changes on your data readings.
11. Now that the electronics are working build the mechanical part and put all together.
12. Calibrate the scale using the 'calibrate scale' sketch from the [HX711 library](https://github.com/bogde/HX711).
13. Put a crate with some ClubMate bottles in it and verify if the number of bottles reported by the scale matches with reality.
        
##     Documentation

1. [GitHub repository of the project](https://github.com/relayr/weekly-workshops/tree/master/biz-demos/smart-crate).
2. [HX711 library](https://github.com/bogde/HX711).
3. [Sparkfun breakout board and introductory info](https://learn.sparkfun.com/tutorials/load-cell-amplifier-hx711-breakout-hookup-guide).
4. [HX711 datasheet](https://cdn.sparkfun.com/datasheets/Sensors/ForceFlex/hx711_english.pdf).
5. [Mechanical setup picture](https://cdn.sparkfun.com/assets/learn_tutorials/3/8/3/HX711_and_Combinator_board_hook_up_guide-02.jpg).
    
##     License
    
The MIT License (MIT)
Copyright (c) 2016 relayr Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
