/* Wemos -- Scale(HX711)
    
    Wemos - HX711
      5V - VCC
      D1 - SCK
      D2 - DT
      GND - G

    HX711 - Loadcell
      E+ _ red
      E- - black
      A- - white
      A+ - green

*/


//Have a look at the onboarding tutorial at the relayr Github:
//https://github.com/relayr/ESP8266_Arduino

#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

//Library for the HX711 amplifier
#include "HX711.h"


// Library for the MQTT protocol
// Please use this fork if you experience problems with the original library:
// https://github.com/uberdriven/pubsubclient
#include <PubSubClient.h>


// WiFi credentials
#define SSID "INTRODUCE_YOUR_SSID_HERE"
#define PASSWORD "INTRODUCE_YOUR_PASSWORD_HERE"


// Credentials from the developer dashboard
#define DEVICE_ID "INTRODUCE_YOUR_DEVICE_ID_HERE"
#define MQTT_USER "INTRODUCE_YOUR_MQTT_USER_HERE"
#define MQTT_PASSWORD "INTRODUCE_YOUR_MQTT_PASSWORD_HERE"
#define MQTT_CLIENTID "INTRODUCE_YOUR_MQTT_CLIENT_HERE" //It can be anything else
#define MQTT_TOPIC "INTRODUCE_YOUR_MQTT_TOPIC_HERE"
#define MQTT_SERVER "INTRODUCE_YOUR_GATEWAY_IP_ADDRESS_HERE"

//This creates the WiFi client and the pub-sub client instance
WiFiClient espClient;
PubSubClient client(espClient);


//Calibration values to be obtained using the SparkFun_HX711_Calibration sketch
#define calibration_factor  "INTRODUCE_YOUR_VALUE_HERE" 
#define zero_factor         "INTRODUCE_YOUR_VALUE_HERE"


//Some definitions:
#define DOUT  D2    
#define CLK  D1
HX711 scale(DOUT, CLK);

float weight, bottles, bottles_p;
unsigned long lastPublishTime = 0;
int publishingPeriod = 1000;

float emptyCrate , emptyBottle, fullBottle ;
int  capCrate ;

const char* product; 
int selProduct = 1; // the default product is ClubMate
bool onlyWeighting = false;

//Function prototypes
void setup_wifi();
void mqtt_connect();
void callback(char* topic, byte* payload, unsigned int length);
void handlePayload(char* payload);
void publish();


//------------------------------------------------------------------------------------//
// Setup function: Configuration and initialization                                   //
//------------------------------------------------------------------------------------//

void setup()
{
  Serial.begin(9600);
  Serial.println("");
  Serial.println("Hello there, I'm your ESP8266.");
  Serial.println("Let's talk to the relayr Cloud!");

  // The scale doesn't need to be tared in this setup. All that belongs to the SparkFun_HX711_Calibration sketch
  scale.set_scale(calibration_factor);
  scale.set_offset(zero_factor); //Zero out the scale using a previously known zero_factor

  setProduct();

  setup_wifi();

  client.setServer(MQTT_SERVER, 1883);
  client.setCallback(callback);

  //200ms is the minimum publishing period
  publishingPeriod = publishingPeriod > 200 ? publishingPeriod : 200;
  mqtt_connect();
}


//------------------------------------------------------------------------------------//
// This function connects to the WiFi network, and prints the current IP address      //
//------------------------------------------------------------------------------------//

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(SSID);

  WiFi.begin(SSID, PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


//------------------------------------------------------------------------------------//
// Callback function, necessary for the MQTT communication                            //
//------------------------------------------------------------------------------------//

void callback(char* topic, byte* payload, unsigned int length)
{
  //Store the received payload and convert it to string
  char p[length + 1];
  memcpy(p, payload, length);
  p[length] = NULL;
  //Print the topic and the received payload
  Serial.println("topic: " + String(topic));
  Serial.println("payload: " + String(p));
  //Call our method to parse and use the received payload
  handlePayload(p);
}


//------------------------------------------------------------------------------------//
// This processes the payload; commands and configurations should be implemented here //
//------------------------------------------------------------------------------------//

void handlePayload(char* payload)
{
  StaticJsonBuffer<200> jsonBuffer;
  //Convert payload to json
  JsonObject& json = jsonBuffer.parseObject(payload);

  if (!json.success())
  {
    Serial.println("json parsing failed");
    return;
  }

  //Get the value of the key "name", aka. listen to incoming commands and configurations
  const char* command = json["name"];
  Serial.println("parsed command: " + String(command));

  //COMMAND "Product": We can change the product we are working with
  if (String(command).equals("Product"))
  {
//    const char* selProduct = json["value"];
    selProduct = json["value"];
    //Serial.println("digital output 1: " + String(selProduct));
    String s(selProduct);
    setProduct();
  }

  //COMMAND "d2": We can toggle the digital output 2
  if (String(command).equals("d2"))
  {
    const char* d2 = json["value"];
    Serial.println("digital output 2: " + String(d2));
    String s(d2);

    if (s.equals("high"))
      digitalWrite(D2, HIGH);

    else if (s.equals("low"))
      digitalWrite(D2, LOW);
  }

  //CONFIGURATION "frequency": We can change the publishing period
  if (String(command).equals("frequency"))
  {
    int frequency = json["value"];

    if ( (frequency >= 200) && (frequency <= 5000) )
    {
      Serial.println("Adjusting publishing period (ms): " + String(frequency));
      publishingPeriod = frequency;
    }

    else
      Serial.println("The requested publishing period is out of the defined range!");
  }
}


//------------------------------------------------------------------------------------//
// This function establishes the connection with the MQTT server                      //
//------------------------------------------------------------------------------------//

void mqtt_connect()
{
  Serial.println("");
  Serial.println("Connecting to MQTT server...");

  if (client.connect(MQTT_CLIENTID, MQTT_USER, MQTT_PASSWORD))
  {
    Serial.println("Connection successful! Subscribing to topic...");
    //Subscribing to the topic "cmd", so we can listen to commands
    client.subscribe("/v1/"DEVICE_ID"/cmd");
    //And to "config" as well, for the configurations
    client.subscribe("/v1/"DEVICE_ID"/config");
  }

  else
  {
    Serial.println("Connection failed! Check your credentials or the WiFi network");
    //This reports the error code
    Serial.println("rc = ");
    Serial.print(client.state());
    //And it tries again in 5 seconds
    Serial.println("Retrying in 5 seconds...");
    delay(5000);
  }
}


//------------------------------------------------------------------------------------//
// This is the MAIN LOOP, it's repeated until the end of time! :)                     //
//------------------------------------------------------------------------------------//

void loop()
{
  //When counter reaches the maximum specified in the default model, it's reset


  //If we're connected, we can send data...
  if (client.connected())
  {
    client.loop();
    //Publish within the defined publishing period
    if (millis() - lastPublishTime > publishingPeriod)
    {
      lastPublishTime = millis();
      //Publishing...

      weight = scale.get_units(); // get weight in lbs

      if (weight < 0) { weight = 0 ; }
      
      weight = 453.592 * weight; // convert lbs to grams
      
      if (onlyWeighting ==false)
      {
        //Get number of Bottles
        bottles = weight - emptyCrate; // net weight
        bottles = bottles/fullBottle; // number of full bottles that would make this weight
        bottles = floor(bottles + 0.5); // round to next number
        if (bottles < 0) { bottles = 0 ; } // in case you leave the crate alone
        // show it in percentage
        bottles_p = (bottles/capCrate) * 100;  
      }
      else if (onlyWeighting ==true)
      {
        bottles = 0;
        bottles_p = 0;
      }

      weight = weight / 1000; // convert grams to kg
      delay(100);
      publish();

    }
  }

  //If the connection is lost, then reconnect...
  else
  {
    Serial.println("Retrying...");
    mqtt_connect();
  }

  //This function prevents the device from crashing
  //since it allows the ESP8266 background functions to be executed
  //(WiFi, TCP/IP stack, etc.)
  yield();
}




//------------------------------------------------------------------------------------//
// Publish function: What we want to send to the relayr Cloud                         //
//------------------------------------------------------------------------------------//

void publish()
{
  //MQTT_MAX_PACKET_SIZE is defined in "PubSubClient.h", it's 128 bytes by default
  //A modified version with 512 bytes it's available here:
  //   https://github.com/uberdriven/pubsubclient
  StaticJsonBuffer<MQTT_MAX_PACKET_SIZE> pubJsonBuffer;
  //Create our JsonArray
  JsonArray& root = pubJsonBuffer.createArray();

  //-------------------------------------------------
  //First object:
  JsonObject& leaf1 = root.createNestedObject();
  //This is how we name what we are sending
  leaf1["meaning"] = "Weight (kg)";
  leaf1["value"] = weight;
  //-------------------------------------------------

  //-------------------------------------------------
  //Second object: 
  JsonObject& leaf2 = root.createNestedObject();
  //This is how we name what we are sending
  leaf2["meaning"] = "Bottles";
  leaf2["value"] = bottles;
  //-------------------------------------------------
  
  //-------------------------------------------------
  //Third object: 
  JsonObject& leaf3 = root.createNestedObject();
  //This is how we name what we are sending
  leaf3["meaning"] = "Bottles in %";
  leaf3["value"] = bottles_p;
  //-------------------------------------------------

  //-------------------------------------------------
  //Fourth object: 
  JsonObject& leaf4 = root.createNestedObject();
  //This is how we name what we are sending
  leaf4["meaning"] = "Product";
  leaf4["value"] = String(product);
  //-------------------------------------------------

  char message_buff[MQTT_MAX_PACKET_SIZE];
  root.printTo(message_buff, sizeof(message_buff));
  client.publish("/v1/"DEVICE_ID"/data", message_buff);
  Serial.println("Publishing " + String(message_buff));

}

//------------------------------------------------------------------------------------------//
// setProduct() function: Configure product settings. Like weight of the crate and elements.//
// Here you modify the products according to what you want to use the scale for.            //
// This can also be understood as a look up table                                           //
//------------------------------------------------------------------------------------------//

void setProduct()
{

  if (selProduct== 1) //ClubMate
  {
      Serial.println("Config changed to ClubMate");
      onlyWeighting = false;
      emptyCrate  = 1880 ;
      emptyBottle = 373.3 ;
      fullBottle  = 893.3 ;
      capCrate  = 20 ;
      product  = "-- Club Mate --" ; 
  }
  else if (selProduct== 2) //Rothaus
  {
      Serial.println("Config changed to Rothaus");   
      onlyWeighting = false; 
      emptyCrate  = 1910 ;
      emptyBottle = 307.5 ;
      fullBottle  = 665 ;
      capCrate  = 24 ;
      product  = "-- Rothaus --" ;  
  }
  else if (selProduct== 3) //Only Weighting
  {
      Serial.println("Config changed to Only Weighting");    
      onlyWeighting = true;
      product  = "-- Only Weighting --" ;  
  }
  else
  {
      Serial.println("Tried to change Config. Unsuccessful. Only Weighting");
      onlyWeighting = true;
      product  = "-- No selection matched --" ; 
  }

}
