//Required libraries
#include "MPU6050/MPU6050.h" //This one is for the accel/gyro
#include "MQTT/MQTT.h"
#include "SparkJson/SparkJson.h"


//Credentials from the developer dashboard
#define DEVICE_ID "8fb4c931-f817-4399-a0f8-69530a24dffe" 
#define MQTT_USER "8fb4c931-f817-4399-a0f8-69530a24dffe" 
#define MQTT_PASSWORD "3FGOGiAPx.sQ"
#define MQTT_CLIENTID "vw-demo" //This can be anything else
#define MQTT_SERVER "mqtt.relayr.io"

//Some definitions, including the publishing period
const int led = D7;
int ledState = LOW;
unsigned long lastPublishTime = 0;
unsigned long lastBlinkTime = 0;
int publishingPeriod = 1000;

// MPU variables:
MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;

//The values of analog pins 0 and 1 will be stored in these variables
float analog0;
float analog1;

//Initializing some stuff...
void setup()
{
  pins_init();
  Serial.begin(9600);
  
  Wire.begin();
  Serial.println("Initializing I2C devices...");
    accelgyro.initialize();
  
    RGB.control(true);

    Serial.println("Hello there, I'm your Photon... and I'm gonna talk to the relayr Cloud!");
    //Setup our LED pin
    pinMode(led, OUTPUT);
    //200ms is the minimum publishing period
    publishingPeriod = publishingPeriod > 200 ? publishingPeriod : 200;
    mqtt_connect();
}

//This function is called up there; it just initializes some pins
void pins_init()
{
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
    pinMode(A0, INPUT);
    pinMode(A1, INPUT);
}

//This is the callback function, necessary for the MQTT communication
void callback(char* topic, byte* payload, unsigned int length);
//Create our instance of MQTT object
MQTT client(MQTT_SERVER, 1883, callback);
//Implement our callback method that's called on receiving data from a subscribed topic
void callback(char* topic, byte* payload, unsigned int length) {
  //Store the received payload and convert it to string
  char p[length + 1];
  memcpy(p, payload, length);
  p[length] = NULL;
  //Print the topic and the received payload
  Serial.println("topic: " + String(topic));
  Serial.println("payload: " + String(p));
  //Call our method to parse and use the received payload
  handlePayload(p);
}

void handlePayload(char* payload) {
  StaticJsonBuffer<200> jsonBuffer;
  //Convert payload to json
  JsonObject& json = jsonBuffer.parseObject(payload);
  if (!json.success()) {
    Serial.println("json parsing failed");
    return;
  }
  //Get the value of the key "command", aka. listen to incoming commands
  const char* command = json["command"];
  Serial.println("parsed command: " + String(command));
  
  //We can send commands to change the color of the RGB
  if (String(command).equals("color"))
  {
    const char* color = json["value"];
    Serial.println("parsed color: " + String(color));
    String s(color);
    if (s.equals("red")){
      RGB.color(255, 0, 0);
    }
    else if (s.equals("green"))
      RGB.color(0, 255, 0);
    else if (s.equals("blue"))
      RGB.color(0, 0, 255);

  }
}

//This function establishes the connection with the MQTT server
void mqtt_connect() {
  Serial.println("Connecting to MQTT server...");
  if (client.connect(MQTT_CLIENTID, MQTT_USER, MQTT_PASSWORD)) {
    Serial.println("Connection successful! Subscribing to topic...");
    //This one subscribes to the topic "cmd", so we can listen to commands
    client.subscribe("/v1/"DEVICE_ID"/cmd");
  }
  else {
    Serial.println("Connection failed! Check your credentials or WiFi network");
  }
}


//This is for the LED to blink
void blink(int interval) {
  if (millis() - lastBlinkTime > interval) {
    //Save the last time you blinked the LED
    lastBlinkTime = millis();
    if (ledState == LOW)
      ledState = HIGH;
    else
      ledState = LOW;
    //Set the LED with the ledState of the variable:
    digitalWrite(led, ledState);
  }
}


//This is the main loop, it's repeated until the end of time :)
void loop()
{
  //If we're connected, we can send data...
  if (client.isConnected()) {

    client.loop();
    //Publish within the defined publishing period
        if (millis() - lastPublishTime > publishingPeriod) {
            
            lastPublishTime = millis();
            
            //Publishing...
            publish();
            
        }
            
        //Blink LED  
        blink(publishingPeriod / 2);
    }
    else {
    //If connection is lost, then reconnect
        Serial.println("Retrying...");
        mqtt_connect();
    }
        
    //Read the values of analog inputs 0 and 1, and store them in variables
  analog0 = analogRead(A0);
  analog1 = analogRead(A1);
  
    //Reading raw values from the device (accel/gyro)
    //This happens way faster than the publishing method...
    accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    Serial.print("a/g:\t");
    Serial.print(ax); Serial.print("\t");
    Serial.print(ay); Serial.print("\t");
    Serial.print(az); Serial.print("\t");
    Serial.print(gx); Serial.print("\t");
    Serial.print(gy); Serial.print("\t");
    Serial.println(gz);
    
        
}


//Publish function; here we add what we want to send to the cloud
void publish() {
  //Create our JsonArray
  StaticJsonBuffer<300> pubJsonBuffer;
  JsonArray& root = pubJsonBuffer.createArray();

//-------------------------------------------------
  //First object: analog input 0
  JsonObject& leaf1 = root.createNestedObject();
  //This is how we name what we are sending
  leaf1["meaning"] = "accel_x";
  //This contains the readings of the port
//  leaf1["value"] = analog0;
  leaf1["value"] = ax;
//-------------------------------------------------
  
//-------------------------------------------------  
//   //Second object: analog input 1
//   JsonObject& leaf2 = root.createNestedObject();
//   //This is how we name what we are sending
//   leaf2["meaning"] = "analog1";
//   //This contains the readings of the port
//   leaf2["value"] = analog1;
//-------------------------------------------------
  
  char message_buff[128];
  root.printTo(message_buff, sizeof(message_buff));
  client.publish("/v1/"DEVICE_ID"/data", message_buff);
//  Serial.println("Publishing " + String(message_buff));

}







